section .text
[bits 32]

;;
;; import / export entries between .nasm and .c parts
;;
%ifidn __OUTPUT_FORMAT__, win32
extern _EntryPoint                  ; import C entry point from main.c
EntryPoint equ _EntryPoint          ; win32 builds from Visual C decorate C names using _
%else
extern EntryPoint                   ; import C entry point from main.c
%endif

global gMultiBootHeader         ; export multiboot structures to .c
global gMultiBootStruct


;;
;; we use hardcoded address space / map for our data structures, the multiboot header and the entry point
;; the plain binary image is loaded to 0x00200000 (2MB)
;;
KERNEL_BASE_VIRTUAL_32      equ 0x40000000			    ; magic 1G VA for x86 builds
KERNEL_BASE_VIRTUAL_64      equ 0x0000000200000000	    ; magic 8G VA for x64 builds
KERNEL_BASE_PHYSICAL        equ 0x200000                ; physical address where this file will be loaded (2 MB PA)

MULTIBOOT_HEADER_BASE       equ KERNEL_BASE_PHYSICAL + 0x400 ; take into account the MZ/PE header + 0x400 allignment
                                                        ; the multiboot header begins in the .text section
MULTIBOOT_HEADER_SIZE       equ 48                      ; check out '3.1.1 The layout of Multiboot header'
MULTIBOOT_HEADER_MAGIC      equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS      equ 0x00010003              ; 0x1 ==> loading of modules must pe 4K alligned, 0x2 ==> OS needs memory map
                                                        ; 0x10000 ==> OS image has valid header_addr, load_addr, ..., entry_addr

MULTIBOOT_INFO_STRUCT_BASE  equ MULTIBOOT_HEADER_BASE + MULTIBOOT_HEADER_SIZE
MULTIBOOT_INFO_STRUCT_SIZE  equ 90

MULTIBOOT_ENTRY_POINT       equ (gMultiBootEntryPoint - gMultiBootHeader) + KERNEL_BASE_PHYSICAL + 0x400

IA32_EFER                   equ 0xC0000080
CR4_PAE                     equ 0x00000020
CR0_PG						equ  1 << 31
CR0_PM						equ  1 << 0
IA23_EFER_LME               equ 0x100


TOP_OF_STACK_VIRTUAL        equ KERNEL_BASE_VIRTUAL_64 + 0x10000



;;
;; KERNEL_BASE_PHYSICAL + 0x400
;;
;; *** IMPORTANT: __init.nasm MUST be the first object to be linked into the code segment ***
;;

gMultiBootHeader:                                       ; check out '3.1.1 The layout of Multiboot header'
.magic          dd MULTIBOOT_HEADER_MAGIC
.flags          dd MULTIBOOT_HEADER_FLAGS
.checksum       dd 0-(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)
.header_addr    dd MULTIBOOT_HEADER_BASE
.load_addr      dd KERNEL_BASE_PHYSICAL
.load_end_addr  dd 0
.bss_end_addr   dd 0
.entry_addr     dd MULTIBOOT_ENTRY_POINT
.mode_type      dd 0
.width          dd 0
.height         dd 0
.depth          dd 0

gMultiBootStruct:                                       ; reserve space for the multiboot info structure (will copy here)
times MULTIBOOT_INFO_STRUCT_SIZE db 0                   ; check out '3.3 Boot information format'

;; leave 0x40 bytes for GDT stuff
times (0x100 - MULTIBOOT_HEADER_SIZE - MULTIBOOT_INFO_STRUCT_SIZE - 0x40) db 0

	GDT64:                           ; Global Descriptor Table (64-bit).
    .Null: equ $ - GDT64         ; The null descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0                         ; Access.
    db 0                         ; Granularity.
    db 0                         ; Base (high).
    .Code: equ $ - GDT64         ; The code descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10011010b                 ; Access (exec/read).
    db 00100000b                 ; Granularity.
    db 0                         ; Base (high).
    .Data: equ $ - GDT64         ; The data descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10010010b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
    .Pointer:                    ; The GDT-pointer.
    dw $ - GDT64 - 1             ; Limit.
    dq gdtBase                     ; Base.
	
	gdtBase equ 0x2004c0


;;
;; N.B. here we have plenty of space (over 60KB to be used to define various data structures needed, e.g. page tables)
;; 
;; Setting up the addreses of page tables 4Kb aligned. 

PML4 equ 0x1000
PDP equ 0x2000
PD equ 0x3000

;;
;; KERNEL_BASE_PHYSICAL + 0x4C0
;;


;;
;; TOP-OF-STACK is KERNEL_BASE_PHYSICAL + 0x10000
;;


;;
;; N.B. multiboot starts in 32 bit PROTECTED MODE, without paging beeing enabled (FLAT); check out '3.2 Machine state' from docs
;; we explicitly allign the entry point to +64 KB (0x10000)
;;

times 0x10000-0x400-$+gMultiBootHeader db 'G'           ; allignment

;;
;; KERNEL_BASE_PHYSICAL + 0x10000
;;
[bits 32]
gMultiBootEntryPoint:
    cli

    MOV     DWORD [0x000B8000], 'O1S1'
%ifidn __OUTPUT_FORMAT__, win32
    MOV     DWORD [0x000B8004], '3121'                  ; 32 bit build marker
%else
    MOV     DWORD [0x000B8004], '6141'                  ; 64 bit build marker
%endif

    ;; enable SSE instructions (CR4.OSFXSR = 1)
    MOV     EAX, CR4
    OR      EAX, 0x00000200
    MOV     CR4, EAX

    MOV     ESP, TOP_OF_STACK_VIRTUAL                   ; temporary ESP, just below code
    

	;; We will clear the tables
	mov edi, PML4
    mov cr3, edi
    xor eax, eax
    mov ecx, 3072
    rep stosd
    mov edi, cr3


	;; set PML4[0] = PDP
    mov eax, PDP
	or eax, 0x3					; set P+W bits
    mov [PML4], eax

    ;; set PDP[0] = PD
    mov eax, PD
    or eax, 0x3					; set P+W bits
    mov [PDP], eax
 
	;; We will map the firts GB of physical memory 
     mov ecx, 512        
map_firstGb:
     mov eax, 0x200000		;; 2 MB pages
     mul ecx            
     or eax, 0x83			
     mov [PD + ecx * 8], eax 
     loop map_firstGb

	 ;; We need to map also the first 2 MB of memory
	 mov eax, 0x200000  
     mul ecx            
     or eax, 0x83      
     mov [PD], eax 

	; set address of P4 table into cr3
	 mov eax, PML4
	 mov cr3, eax

	;;Enable PAE-paging by setting the PAE-bit in the fourth control register:
	
	mov eax, cr4                 ; Set the A-register to control register 4.
	or eax, CR4_PAE              ; Set the PAE-bit, which is the 6th bit .
	mov cr4, eax                 ; Set control register 4 to the A-register.

	;;set the LM-bit

	mov ecx, IA32_EFER           ; Set the C-register to 0xC0000080, which is the EFER MSR.
    rdmsr                        ; Read from the model-specific register.
    or eax, IA23_EFER_LME        ; Set the LM-bit which is the 9th bit (bit 8).
    wrmsr                        ; Write to the model-specific register.

	;;Enabling paging and protected mode:

	mov eax, cr0                 ; Set the A-register to control register 0.
    or eax, CR0_PG               ; Set the PG-bit, which is the 31nd bit, and the PM-bit, which is the 0th bit.
    or eax, CR0_PM
	mov cr0, eax                 ; Set control register 0 to the A-register.

	;; Get ready for the jumt to LM 

	lgdt [0x2004d8]		        ; Load the 64-bit global descriptor table.
    jmp 0x08:0x2100b9			; Set the code segment and enter 64-bit long mode.
	
	[bits 64]
LM_Section:
	
   mov ax, 0
   mov ss, ax
   mov ds, ax
   mov es, ax
   mov fs, ax
   mov gs, ax

    CALL EntryPoint
    
	call __magic

	mov edi, 0xB8000              ; Set the destination index to 0xB8000.
    mov rax, 0x1F201F201F201F20   ; Set the A-register to 0x1F201F201F201F20.
    mov ecx, 500                  ; Set the C-register to 500.
    rep stosq                     ; Clear the screen.
	
	hlt

;;--------------------------------------------------------
;; EXPORT TO C FUNCTIONS
;;--------------------------------------------------------

;%ifidn __OUTPUT_FORMAT__, win32 ; win32 builds from Visual C decorate C names using _ 
;global ___cli
;___cli equ __cli
;%else
;global __cli
;%endif

%macro EXPORT2C 1-*
%rep  %0
    %ifidn __OUTPUT_FORMAT__, win32 ; win32 builds from Visual C decorate C names using _ 
    global _%1
    _%1 equ %1
    %else
    global %1
    %endif
%rotate 1 
%endrep
%endmacro

EXPORT2C __cli, __sti, __magic

__cli:
    CLI
    RET

__sti:
    STI
    RET

__magic:
    XCHG    BX,BX
    RET
