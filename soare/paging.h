#pragma once
#ifndef _PAGING_H_
#define _PAGING_H_


#define KERNEL_BASE_VIRTUAL_32  0x40000000			// magic 1G VA for x86 builds
#define KERNEL_BASE_VIRTUAL_64  0x0000000200000000  // magic 8G VA for x64 builds
#define KERNEL_BASE_PHYSICAL 0x200000				// physical address where this file will be loaded(2 MB PA)



#endif