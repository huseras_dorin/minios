#include "screen.h"

static PSCREEN gVideo = (PSCREEN)(0x000B8000);

int currentPosition = 0;
BYTE defaultColor = 10;

void HelloBoot()
{
    int i, len;
	char boot[] = "Hello Boot";

	len = 0;
	while (boot[len] != 0)
	{
		len++;
	}

	for (i = 0; (i < len) && (i < MAX_OFFSET); i++)
	{
		gVideo[i].color = 10;
		gVideo[i].c = boot[i];
	}
	currentPosition = i;
}

void SetColor(BYTE Color) {
	int i;
	defaultColor = Color;

	for ( i = 0; i < MAX_OFFSET; i++)
	{
		gVideo[i].color = defaultColor;
	}
}

void ClearScreen() {
	int i;
	for (i = 0; i < MAX_OFFSET; i++)
	{
		gVideo[i].c = 0;
	}
	currentPosition = 0;
}

void scroll() {
	for (int i = 0; i < MAX_OFFSET - MAX_COLUMNS; i++) {
		gVideo[i].c = gVideo[i + MAX_COLUMNS].c;
	}

	for (int i = MAX_OFFSET - MAX_COLUMNS; i < MAX_OFFSET; i++) {
		gVideo[i].c = 0;
	}
}

void PutChar(char C, int Pos) {

	if (C == '\n') {
		currentPosition += MAX_COLUMNS - currentPosition % MAX_COLUMNS - 1;
	}
	else {
		if (Pos < MAX_OFFSET) {
			gVideo[Pos].c = C;
			gVideo[Pos].color = defaultColor;
		}
	}	
}

void PutString(char* String) {

	for (int i = 0; (String[i] != 0); i++)
	{
		if ((currentPosition >= MAX_OFFSET)) {
			scroll();
			currentPosition -= MAX_COLUMNS;
		}
		PutChar(String[i],currentPosition);
		currentPosition++;
	}
}

void PutStringFrom(char* String,int Pos) {
	

	for (int i = 0; (String[i] != 0) && (i < MAX_OFFSET); i++)
	{
		PutChar(String[i], Pos + i);
	}
}

void PutStringLine(char *String, int Line)
{
	PutStringFrom(String, Line * MAX_COLUMNS);
}


