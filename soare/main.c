#include "boot.h"
#include "screen.h"
#define MAX_ENTRY_SIZE 512 
#define MAX_PAGE_SIZE  4096
#define NOT_PRESENT 0x00000002
#define PAGE_INIT 0x1000
typedef __int32 int32_t;
typedef __int64 int64_t;
typedef unsigned __int32 uint32_t;

void EntryPoint(void)
{
   // __magic();                  // break into BOCHS
	ClearScreen();
	HelloBoot();
}
